<!DOCTYPE html>
<html>
<head>
	<title>DOI Code Exercise</title>
	<link href="style.css" rel="stylesheet" type="text/css" />
	<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
</head>
<body>

<?php include_once './include.php'; ?>

<div id="content_wrapper">
	<h1>DOI Code Exercise</h1>

	<?php
	if (!file_exists('./data')) {
		get_data();
	}

	$articles_sorted = [];
	$files = scandir('./data');
	foreach ($files as $file) {
		if (!in_array($file, ['.', '..'])) {
			
			$data = json_decode(file_get_contents('./data/' . $file));

			$publisher = $data->message->publisher;
			$title = $data->message->title[0];
			$authors = prepare_authors($data->message->author);

			$articles_sorted[$publisher][$title] = array(
				'type' => $data->message->type,
				'url' => $data->message->URL,
				'author' => $authors
			);
			
		}
	}

	ksort($articles_sorted);
	foreach ($articles_sorted as $publisher => $article_list) {
		ksort($article_list);
		?>

		<div class="publisher_wrapper">
			<h2><?php echo $publisher; ?></h2>
			<div class="article_list_wrapper">
				<?php 
				foreach ($article_list as $title => $details) {
					?>
					<div class="article_wrapper <?php echo $details['type']; ?>">
						<a href="<?php echo $details['url']; ?>" target="_blank">
							<h3><?php echo $title; ?></h3>
						</a>
						<?php if (!empty($details['author'])) { ?>
							<p>
								<?php echo $details['author']; ?></p>
						<?php } ?>
					</div>		
				<?php } ?>
			</div>
		</div>

	<?php } ?>

</div> <!-- END content_wrapper -->

</body>
</html>
