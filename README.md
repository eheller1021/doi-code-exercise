# DOI Exercise

DOI Exercise is a PHP code sample that pulls journal data from the Crossref API.

## Requirements

- PHP 5.4+

## Usage

```php
cd /path/to/folder
php -S localhost:8000
```
