<?php

/**
 * Gets data from Crossref API and saves to "data" folder.
 */
function get_data() {
	mkdir('./data');
	$dois = json_decode(file_get_contents('./dois.json'));
	$counter = 0;
	foreach($dois as $doi) {
		file_put_contents('./data/' . $counter . '.json', file_get_contents('http://api.crossref.org/works/' . $doi));
		$counter++;
	}
}

/**
 * Formats author data for a single article.
 *
 * @param array $authors
 *  Author data from Crossref
 *
 * @return string
 *  List of author last names
 */
function prepare_authors($authors) {
	$authors_arr = [];
	if (!empty($authors)) {
		foreach ($authors as $author) {
			$authors_arr[] = $author->family;
		}
	}
	$authors_str = implode(', ', $authors_arr);
	return strlen($authors_str) > 100 ? substr($authors_str, 0, 100) . '...' 
		: $authors_str;
}
